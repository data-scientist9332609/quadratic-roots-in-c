#include <stdio.h>
#include <complex.h>
#include <math.h>

void findRoots(double a, double b, double c, double *root1, double *root2) {
    double discriminant = b * b - 4.0 * a * c;

    if (discriminant > 0) {
        *root1 = (-b + sqrt(discriminant)) / (2 * a);
        *root2 = (-b - sqrt(discriminant)) / (2 * a);
        printf("Roots are real and distinct.\n");
    } else if (discriminant == 0) {
        *root1 = -b / (2 * a);
        printf("Roots are real and equal.\n");
    } else {
        double realPart = -b / (2 * a);
        double imaginaryPart = sqrt(-discriminant) / (2 * a);
        *root1 = realPart + I * imaginaryPart;
        *root2 = realPart - I * imaginaryPart;
        printf("Roots are complex.\n");
    }
}

int main() {
    double a, b, c, root1, root2;

    printf("Enter coefficients a, b, and c: ");
    scanf("%lf %lf %lf", &a, &b, &c);

    if (a == 0) {
        printf("Error: a cannot be zero.\n");
        return 1;
    }

    findRoots(a, b, c, &root1, &root2);

    if (root1 == root2)
        printf("Root = %.2lf\n", root1);
    else
        printf("Root1 = %.2lf, Root2 = %.2lf\n", root1, root2);

    return 0;
}

